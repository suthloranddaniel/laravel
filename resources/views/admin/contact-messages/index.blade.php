@extends('layouts.master')

@section('content')
@auth
be van lépve
@else
nincs belépve
@endauth
<div class="row">
    <div class="col-xs-12" >
        <a  href="" class="btn btn-primary btn-lg">Új felvitele</a>
    </div>
    <div class=" col-xs-12">
        <table class="table-bordered table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>név</th>
                    <th>email</th>
                    <th>üzenet</th>
                    <th>művelet</th>  
                </tr>
            </thead>
            <tbody>
                @forelse($contact_messages as $message)
                <tr>
                    <td>{{$message->id}}</td>
                    <td>{{$message->name}}</td>
                    <td>{{$message->email}}</td>
                    <td width="400">{{$message->contact_message}}</td>
                    <td nowrap> <a  href="" class="alert alert-success">m</a> |
                        <a  href="" class="alert alert-danger">t</a>  </td>  
                </tr>
                @empty
                <tr>
                    <td colspan='5'>'nincs itt semmi...'</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection