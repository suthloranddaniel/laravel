<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use App\ContactMessage as CM;

class PageController extends Controller
{
    public function contact(){
        return view('contact');
    }
    
    public function processContactform(Request $request){
        $rules = [
            'email' => 'required|email',
            'name' => 'required|min:3',
            'contact_message' => 'required|min:10'
         ];
        $validator = Validator::make($request->all(),$rules);
        
        if($validator->fails()){
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        //ide csak ellenőrzésen átesett jó adatok esetén kerülünk
        //
        //mentsük el adatbázisba az üzenet elemeit
        $cm = CM::create($request->all());
        
        //küldjünk egy emailt a tartalommal
        $replyTo=$request->get('email');
        
        Mail::send('emails.contactmail', ['data'=>$request->all()], function ($m) use ($replyTo){
            $m->from('info@yoursitedomain.com', env('APP_NAME'));
            $m->replyTo($replyTo);
            $m->to('info@yoursitedomain.com', 'sajátmagam')->subject('Üzenet az oldalról!');
        });
        
        //Irányítsuk a felhasználót a contact formra sikeres üzenteküldés üzenettel
        
        $request->session()->flash('status', 'Sikeres üzenetküldés, köszönjük');//eltároljuk a köv oldalbetöltésig a sessionbe az üzenetet amit a felh.-nak ki akarunk írni

        return redirect()->back();
    }
}
