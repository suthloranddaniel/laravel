<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactMessage as CM;

class AdminController extends Controller
{
    public function dashboard(){
        //összes contact message kilistázása
        $contact_messages = CM::all();
       return view('admin.dashboard');
    }
    
    public function contactMessages(){
        //összes contact message kilistázása
        $contact_messages = CM::all();
       return view('admin.contact-messages.index',compact('contact_messages'));
    }
}
